package com.example.aviatorpopv2.screens.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class GameViewModel: ViewModel() {

    private val _introPopup = MutableStateFlow(true)
    val introPopup = _introPopup.asStateFlow()

    private val _endPopup = MutableStateFlow<Int?>(null)
    val endPopup:StateFlow<Int?> = _endPopup.asStateFlow()

    private val _gameRestart = MutableSharedFlow<Boolean>()
    val gameRestart = _gameRestart.asSharedFlow()

    fun endRun(distance: Int) {
        viewModelScope.launch {
            _endPopup.emit(distance)
        }
    }

    fun startGame() {
        if(introPopup.value) {
            viewModelScope.launch {
                _introPopup.emit(false)
            }
        }
        viewModelScope.launch {
            _endPopup.emit(null)
        }
        viewModelScope.launch {
            _gameRestart.emit(true)
        }
    }
}
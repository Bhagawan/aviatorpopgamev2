package com.example.aviatorpopv2.screens

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import im.delight.android.webview.AdvancedWebView

@Composable
fun AviatorPopWebViewScreen(webView: AdvancedWebView, homeUrl: String) {
    AndroidView(factory = { webView },
        modifier = Modifier.fillMaxSize(),
        update = {
            it.loadUrl(homeUrl)
        })
}
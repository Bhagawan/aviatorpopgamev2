package com.example.aviatorpopv2.util

import androidx.annotation.Keep

@Keep
data class AviatorPopSplashResponse(val url : String)
package com.example.aviatorpopv2.util

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.aviatorpopv2.MainViewModel
import com.example.aviatorpopv2.screens.AviatorPopSplashScreen
import com.example.aviatorpopv2.screens.AviatorPopWebViewScreen
import com.example.aviatorpopv2.screens.Screens
import com.example.aviatorpopv2.screens.game.AviatorPopGameScreen
import im.delight.android.webview.AdvancedWebView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun NavigationComponent(navController: NavHostController, webView: AdvancedWebView, viewModel: MainViewModel = viewModel()) {
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_SCREEN.label
    ) {
        composable(Screens.SPLASH_SCREEN.label) {
            AviatorPopSplashScreen()
        }
        composable(Screens.WEB_VIEW.label) {
            AviatorPopWebViewScreen(webView, viewModel.url)
        }
        composable(Screens.GAME.label) {
            AviatorPopGameScreen()
        }
    }
    LaunchedEffect("nav") {
        viewModel.navigationFlow.onEach {
            if(it == Screens.WEB_VIEW || it == Screens.GAME) navController.backQueue.clear()
            navController.navigate(it.label)
        }.launchIn(this)
    }
}
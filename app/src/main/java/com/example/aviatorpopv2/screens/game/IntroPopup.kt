package com.example.aviatorpopv2.screens.game

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.TabRowDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import com.example.aviatorpopv2.R
import com.example.aviatorpopv2.ui.theme.Green
import com.example.aviatorpopv2.ui.theme.Grey
import com.example.aviatorpopv2.ui.theme.Orange

@Composable
fun IntroPopup() {
    Box(modifier = Modifier.fillMaxSize()) {
        Column(modifier = Modifier
            .wrapContentSize()
            .align(Alignment.Center)
            .padding(Dp(10.0f))
            .border(Dp(3.0f), color = Green, shape = RoundedCornerShape(Dp(20.0f)))
            .background(color = Grey, shape = RoundedCornerShape(Dp(20.0f)))) {
            Text(text = stringResource(id = R.string.game_intro), fontSize = TextUnit(20.0f, TextUnitType.Sp), textAlign = TextAlign.Center, color = Color.White, modifier = Modifier
                .align(Alignment.CenterHorizontally).sizeIn(maxWidth = Dp(300.0f)))
            Box(modifier = Modifier
                .height(Dp(22.0f))
                .wrapContentSize()
                .padding(Dp(10.0f))) {
                TabRowDefaults.Divider(color = Green, thickness = Dp(3.0f), modifier = Modifier.sizeIn(maxWidth = Dp(300.0f)))
            }
            Column(modifier = Modifier
                .wrapContentSize()
                .padding(vertical = Dp(3.0f))
                .background(color = Color.Black, shape = RoundedCornerShape(Dp(10.0f)))
                .padding(Dp(5.0f))
                .align(Alignment.CenterHorizontally)) {
                Image(painter = painterResource(id = R.drawable.ic_arplanev2), contentDescription = "", modifier = Modifier.sizeIn(maxHeight = Dp(50.0f)))
                Box(modifier = Modifier.background(color = Orange, shape = CircleShape).size(Dp(50.0f)).align(
                    Alignment.CenterHorizontally)) { }
            }
        }
    }
}
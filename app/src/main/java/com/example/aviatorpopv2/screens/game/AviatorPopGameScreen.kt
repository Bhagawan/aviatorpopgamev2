package com.example.aviatorpopv2.screens.game

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.aviatorpopv2.game.AviatorPopView
import com.example.aviatorpopv2.ui.theme.Grey
import com.example.aviatorpopv2.util.Prefs
import com.example.aviatorpopv2.util.Url_background
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun AviatorPopGameScreen() {
    val viewModel :GameViewModel =  viewModel()
    var gameBackground by remember { mutableStateOf<ImageBitmap?>(null) }
    DisposableEffect("logo") {
        val picasso = Picasso.get()

        val target = object : Target {
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) { }
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                gameBackground = bitmap?.asImageBitmap()
            }
        }

        picasso.load(Url_background).into(target)

        onDispose {
            picasso.cancelRequest(target)
        }
    }

    Box(modifier = Modifier
        .fillMaxSize()
        .background(Grey)) {
        gameBackground?.let { Image(it, "back",modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillHeight) }

        AndroidView(factory = { AviatorPopView(it) },
            modifier = Modifier.fillMaxSize(),
            update = {
                viewModel.gameRestart.onEach {restart -> if(restart) it.restart() }.launchIn(viewModel.viewModelScope)
                it.setInterface(object : AviatorPopView.GameInterface {
                    override fun onFinish(distance: Int) {
                        viewModel.endRun(distance)
                        if(Prefs.loadRecord(it.context) < distance / 10) Prefs.saveRecord(it.context, distance / 10)
                    }
                })
            })

    }

    val introPopup = viewModel.introPopup.collectAsState()
    val endPopup = viewModel.endPopup.collectAsState()

    if(introPopup.value && endPopup.value == null) {
        Box(modifier = Modifier
            .fillMaxSize()
            .clickable { viewModel.startGame() }, contentAlignment = Alignment.Center) {
            IntroPopup()
        }
    }
    if(endPopup.value != null) {
        Box(modifier = Modifier
            .fillMaxSize()
            .clickable { viewModel.startGame() }, contentAlignment = Alignment.Center) {
            endPopup.value?.let {
                EndPopup(it, Prefs.loadRecord(LocalContext.current))
            }
        }
    }
}
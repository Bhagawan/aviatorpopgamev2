package com.example.aviatorpopv2.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)


val Green = Color(0xFF5D8B5F)
val Grey = Color(0xFF5A5A5A)
val Gold = Color(0xFFFFC107)
val Orange = Color(0xFFBF530A)
package com.example.aviatorpopv2.screens

enum class Screens(val label: String) {
    SPLASH_SCREEN("splash"),
    GAME("game"),
    WEB_VIEW("web_view")
}
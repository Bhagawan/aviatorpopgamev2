package com.example.aviatorpopv2.screens.game

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.TabRowDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import com.example.aviatorpopv2.R
import com.example.aviatorpopv2.ui.theme.Gold
import com.example.aviatorpopv2.ui.theme.Green
import com.example.aviatorpopv2.ui.theme.Grey

@Composable
fun EndPopup(distance: Int, record: Int) {
    Box(modifier = Modifier.fillMaxSize()) {
        Column(modifier = Modifier
            .wrapContentSize()
            .align(Alignment.Center)
            .padding(Dp(10.0f))
            .border(Dp(3.0f), color = Green, shape = RoundedCornerShape(Dp(20.0f)))
            .background(color = Grey, shape = RoundedCornerShape(Dp(20.0f)))) {
            Text(text = stringResource(id = R.string.game_end), textAlign = TextAlign.Center, fontSize = TextUnit(30.0f, TextUnitType.Sp), color = Green, modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .wrapContentSize())
            Box(modifier = Modifier
                .height(Dp(22.0f))
                .wrapContentSize()
                .padding(Dp(10.0f))) {
                TabRowDefaults.Divider(color = Green, thickness = Dp(3.0f), modifier = Modifier.sizeIn(maxWidth = Dp(300.0f)))
            }
            Row(modifier = Modifier
                .wrapContentSize()
                .align(Alignment.CenterHorizontally)
                .padding(vertical = Dp(3.0f))) {
                Text(text = stringResource(id = R.string.game_distance), fontSize = TextUnit(15.0f, TextUnitType.Sp), color = Color.White,
                    textAlign = TextAlign.End,
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .weight(1.0f, false)
                        .padding(horizontal = Dp(5.0f)))
                Text(text = distance.toString(), fontSize = TextUnit(15.0f, TextUnitType.Sp), color = Color.White,
                    textAlign = TextAlign.Start,
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .weight(1.0f, false)
                        .padding(horizontal = Dp(5.0f)))
            }
            Row(modifier = Modifier
                .wrapContentSize()
                .align(Alignment.CenterHorizontally)
                .padding(vertical = Dp(3.0f))) {
                Text(text = stringResource(id = R.string.game_record), fontSize = TextUnit(15.0f, TextUnitType.Sp), color = Gold,
                    textAlign = TextAlign.End,
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .weight(1.0f, false)
                        .padding(horizontal = Dp(5.0f)))
                Text(text = record.toString(), fontSize = TextUnit(15.0f, TextUnitType.Sp), color = Gold,
                    textAlign = TextAlign.Start,
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .weight(1.0f, false)
                        .padding(horizontal = Dp(5.0f)))
            }
            Image(painter = painterResource(id = R.drawable.ic_baseline_replay), contentDescription = "", modifier = Modifier
                .size(Dp(100.0f))
                .align(
                    Alignment.CenterHorizontally
                ))
        }
    }
}

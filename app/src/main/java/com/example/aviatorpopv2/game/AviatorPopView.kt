package com.example.aviatorpopv2.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.aviatorpopv2.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue
import kotlin.random.Random

class AviatorPopView(context: Context) : View(context) {
    private var mWidth = 0
    private var mHeight = 0
    private var planeX = 0.0f
    private var planeY = 0.0f
    private var planeHeight = 1.0f
    private var planeWidth = 1.0f

    private var distance = 0

    private val planeBitmap = AppCompatResources.getDrawable(context,R.drawable.ic_arplanev2)?.toBitmap()
    private val restartBitmap = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_replay)?.toBitmap()

    private val targets = ArrayList<GameTarget>()
    private var maxTargetRadius = 100
    private var minTargetRadius = 40

    private var planeFuel = 60

    private var state = PAUSE

    private var gameInterface: GameInterface? = null

    companion object {
        const val PAUSE = 0
        const val GAME = 1
    }

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            planeY = mHeight / 2.0f
            planeX = mWidth / 2.0f
            planeHeight = mWidth / 15.0f
            planeWidth = planeHeight / (planeBitmap?.height ?: 200) * (planeBitmap?.width ?: 130)
            minTargetRadius = mHeight / 25
            maxTargetRadius = mHeight / 15
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(state == GAME) {
                updatePlane()
                updateTargets()
                drawTargets(it)
            }
            drawTerrain(it)
            drawPlane(it)
            drawUI(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> return true
            MotionEvent.ACTION_UP -> {
                if(state == GAME) hit(event.x, event.y)
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public

    fun setInterface(i: GameInterface) {
        gameInterface = i
    }

    fun restart() {
        state = GAME
        planeY = mHeight / 2.0f
        distance = 0
        planeFuel = 60
        targets.clear()
    }


    //// Private

    private fun drawPlane(c: Canvas) {
        val p = Paint()
        if (planeBitmap != null) {
            c.drawBitmap(planeBitmap, null, Rect((planeX - (planeWidth / 2)).toInt(), (planeY - planeHeight / 2).toInt(), (planeX + planeWidth / 2).toInt(), (planeY + planeHeight / 2).toInt()), p)
        }
    }

    private fun drawTerrain(c : Canvas) {
        val p = Paint()
        p.color = ResourcesCompat.getColor(context.resources, R.color.green, null)
        p.style = Paint.Style.FILL
        c.drawRect(0.0f, mHeight - 10.0f, mWidth.toFloat(), mHeight.toFloat(), p)
    }

    private fun drawUI(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.strokeWidth = 3.0f
        p.textSize = 30.0f
        p.textAlign = Paint.Align.CENTER

        if(state == GAME) {
            p.color = Color.WHITE
            p.textSize = 30.0f
            p.isFakeBoldText = true
            c.drawText((distance / 10.0f).toInt().toString(), mWidth / 2.0f, 30.0f, p)
        }
    }

    private fun drawTargets(c : Canvas) {
        val p = Paint()
        for(target in targets) {
            if(target.pressed) {
                p.color = ResourcesCompat.getColor(context.resources, R.color.green, null)
            } else {
                val part : Float = (target.lifespan - target.liveTime).coerceAtLeast(0).toFloat() / target.lifespan
                p.color = Color.argb(255, 228,(123 * part).toInt(),(43 * part).toInt())
            }
            c.drawCircle(target.x.toFloat(), target.y.toFloat(), target.radius.toFloat(), p)
        }
    }

    private fun updatePlane() {
        planeFuel = (planeFuel - 1).coerceAtLeast(0)
        distance++
        if(planeFuel <= 0) planeY++
        if(planeY > mHeight - planeHeight / 2) {
            state = PAUSE
            gameInterface?.onFinish(distance)
        }
    }

    private fun updateTargets() {
        var n = 0
        while(n < targets.size) {
            targets[n].liveTime++
            if(targets[n].pressed) targets[n].radius++
            if(targets[n].liveTime > targets[n].lifespan) {
                targets.removeAt(n)
                n--
            }
            n++
        }

        if(targets.isEmpty() || Random.nextInt(1000) < 8) spawnTarget()
    }

    private fun spawnTarget() {
        var x  = Random.nextInt(maxTargetRadius, mWidth - maxTargetRadius)
        var y  = Random.nextInt(maxTargetRadius + 50, mHeight - maxTargetRadius - 10)
        while(x in (planeX - planeWidth / 2 - maxTargetRadius).toInt()..(planeX + planeWidth / 2 + maxTargetRadius).toInt() &&
            y in (planeY - planeHeight / 2 - maxTargetRadius).toInt()..(planeY + planeHeight / 2 + maxTargetRadius).toInt()) {

            x  = Random.nextInt(maxTargetRadius, mWidth - maxTargetRadius)
            y  = Random.nextInt(maxTargetRadius + 50, mHeight - maxTargetRadius - 10)
        }
        val lifespan = 20 + (180 - distance / 50).coerceAtLeast(0)
        targets.add(GameTarget(x, y, Random.nextInt(minTargetRadius, maxTargetRadius), lifespan))
    }

    private fun hit(x: Float, y: Float) {
        for(target in targets) {
            if((target.x - x).absoluteValue < target.radius && (target.y - y).absoluteValue < target.radius && !target.pressed) {
                target.pressed = true
                target.lifespan = target.liveTime + 30
                planeFuel+=50
            }
        }
    }

    interface GameInterface {
        fun onFinish(distance: Int)
    }
}
package com.example.aviatorpopv2

import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.aviatorpopv2.screens.Screens
import com.example.aviatorpopv2.util.AviatorPopServerClient
import com.onesignal.OneSignal
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class MainViewModel: ViewModel() {
    private var request: Job? = null

    private val _navigationFlow = MutableSharedFlow<Screens>( 1)
    val navigationFlow = _navigationFlow.asSharedFlow()

    var url = ""

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = AviatorPopServerClient.create().getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> switchToGame()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                switchToGame()
                            }
                            else -> viewModelScope.launch {
                                url ="https://${splash.body()!!.url}"
                                _navigationFlow.tryEmit(Screens.WEB_VIEW)
                            }
                        }
                    } else switchToGame()
                } else switchToGame()
            }
        } catch (e: Exception) {
            switchToGame()
        }
    }

    private fun switchToGame() {
        viewModelScope.launch {
            _navigationFlow.tryEmit(Screens.GAME)
        }
    }
}
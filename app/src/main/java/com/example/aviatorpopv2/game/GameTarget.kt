package com.example.aviatorpopv2.game

data class GameTarget(val x: Int, val y: Int, var radius: Int = 40, var lifespan: Int = 240, var liveTime : Int = 0, var pressed: Boolean = false)
